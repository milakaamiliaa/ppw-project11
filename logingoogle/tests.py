from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.apps import apps
from .views import *

class logingoogle(TestCase):
	def test_logingoogle_logout_url_is_exist(self):
		response = Client().get('/logout/')
		self.assertEqual(response.status_code, 302)

	def test_logingoogle_using_logoutView_func(self):
		found = resolve('/logout/')
		self.assertEqual(found.func, logoutView)