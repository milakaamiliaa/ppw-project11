from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'logingoogle'

urlpatterns = [
    path('logout/', views.logoutView, name="logout"),
]