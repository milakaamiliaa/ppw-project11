from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.core import serializers
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt
import requests
import json

def logoutView(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/')