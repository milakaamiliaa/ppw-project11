from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

class campaignpage_test(TestCase):
	def test_if_campaignpage0_url_exist(self):
		response = Client().get('/ayo-bantu-palu-donggala/')
		self.assertEqual(response.status_code, 200)

	def test_if_campaignpage1_url_exist(self):
		response = Client().get('/ulurkan-tangan-untuk-lombok/')
		self.assertEqual(response.status_code, 200)

	def test_if_campaignpage2_url_exist(self):
		response = Client().get('/bantu-korban-tsunami-palu-donggala/')
		self.assertEqual(response.status_code, 200)

	def test_if_campaignpage3_url_exist(self):
		response = Client().get('/bantu-anak-anak-korban-gempa-lombok/')
		self.assertEqual(response.status_code, 200)

	def test_donation_using_campaignpage0_template(self):
		response = Client().get('/ayo-bantu-palu-donggala/')
		self.assertTemplateUsed(response, 'campaignpage.html')

	def test_donation_using_campaignpage1_template(self):
		response = Client().get('/ulurkan-tangan-untuk-lombok/')
		self.assertTemplateUsed(response, 'campaignpage.html')

	def test_donation_using_campaignpage2_template(self):
		response = Client().get('/bantu-korban-tsunami-palu-donggala/')
		self.assertTemplateUsed(response, 'campaignpage.html')

	def test_donation_using_campaignpage3_template(self):
		response = Client().get('/bantu-anak-anak-korban-gempa-lombok/')
		self.assertTemplateUsed(response, 'campaignpage.html')

	def test_donation_using_campaign0_func(self):
		found = resolve('/ayo-bantu-palu-donggala/')
		self.assertEqual(found.func, campaign0)

	def test_donation_using_campaign1_func(self):
		found = resolve('/ulurkan-tangan-untuk-lombok/')
		self.assertEqual(found.func, campaign1)

	def test_donation_using_campaign2_func(self):
		found = resolve('/bantu-korban-tsunami-palu-donggala/')
		self.assertEqual(found.func, campaign2)

	def test_donation_using_campaign3_func(self):
		found = resolve('/bantu-anak-anak-korban-gempa-lombok/')
		self.assertEqual(found.func, campaign3)

	