from django.urls import path
from . import views
from .views import campaign0
from .views import campaign1
from .views import campaign2
from .views import campaign3

app_name = 'campaignpage'

urlpatterns = [
	path('ayo-bantu-palu-donggala/', views.campaign0, name = 'Ayo Bantu Palu-Donggala'),
	path('ulurkan-tangan-untuk-lombok/', views.campaign1, name='Ulurkan Tangan untuk Lombok'),
	path('bantu-korban-tsunami-palu-donggala/', views.campaign2, name='Bantu Korban Tsunami Palu-Donggala'),
	path('bantu-anak-anak-korban-gempa-lombok/', views.campaign3, name='Bantu Anak-Anak Korban Gempa Lombok'),
]
