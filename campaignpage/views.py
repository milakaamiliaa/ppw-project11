from django.shortcuts import render
from donation.models import DonaturEvent
from landingpage.models import Campaigns


def campaign0(request):
    info = {}
    info['title'] = ''
    info['img'] = 'https://i.imgur.com/JC7XxTs.jpg'
    info['desc'] = ''
    info['amount'] = 0
    info['jmldonatur'] = 0
    info['donatur'] = []
    info['progress'] = ""
    allevent = DonaturEvent.objects.all()
    allcampaigns = Campaigns.objects.all()
    for i in allevent:
        if i.namaprogram == 'Ayo Bantu Palu-Donggala':  # pragma: no cover
            info['amount'] += i.amount  # pragma: no cover
            info['jmldonatur'] += 1  # pragma: no cover
            if i.allow:  # pragma: no cover
                info['donatur'].append(i.nama)  # pragma: no cover
            else:  # pragma: no cover
                info['donatur'].append('Anonymous')  # pragma: no cover
    progCount = info['amount'] / 10000000
    info['progress'] = str(progCount)
    for j in allcampaigns:  # pragma: no cover
        if j.title == 'Ayo Bantu Palu-Donggala':  # pragma: no cover
            info['title'] = j.title  # pragma: no cover
            info['desc'] = j.description
    return render(request, 'campaignpage.html', info)


def campaign1(request):
    info = {}
    info['title'] = ''
    info['img'] = "https://i.imgur.com/k7xycbT.jpg"
    info['desc'] = ''
    info['amount'] = 0
    info['jmldonatur'] = 0
    info['donatur'] = []
    info['progress'] = ""
    allevent = DonaturEvent.objects.all()
    allcampaigns = Campaigns.objects.all()  # pragma: no cover
    for i in allevent:  # pragma: no cover
        if i.namaprogram == 'Ulurkan Tangan untuk Lombok':  # pragma: no cover
            info['amount'] += i.amount  # pragma: no cover
            info['jmldonatur'] += 1  # pragma: no cover
            if i.allow:  # pragma: no cover
                info['donatur'].append(i.nama)  # pragma: no cover
            else:
                info['donatur'].append('Anonymous')
    progCount = info['amount'] / 10000000
    info['progress'] = str(progCount)  # pragma: no cover
    for j in allcampaigns:  # pragma: no cover
        if j.title == 'Ulurkan Tangan untuk Lombok':  # pragma: no cover
            info['title'] = j.title  # pragma: no cover
            info['desc'] = j.description
    return render(request, 'campaignpage.html', info)


def campaign2(request):
    info = {}
    info['title'] = ''
    info['img'] = 'https://i.imgur.com/CcizKZa.jpg'
    info['desc'] = ''
    info['amount'] = 0
    info['jmldonatur'] = 0
    info['donatur'] = []
    info['progress'] = ""
    allevent = DonaturEvent.objects.all()  # pragma: no cover
    allcampaigns = Campaigns.objects.all()  # pragma: no cover
    for i in allevent:  # pragma: no cover
        if i.namaprogram == 'Bantu Korban Tsunami Palu-Donggala':  # pragma: no cover
            info['amount'] += i.amount  # pragma: no cover
            info['jmldonatur'] += 1  # pragma: no cover
            if i.allow:  # pragma: no cover
                info['donatur'].append(i.nama)
            else:
                info['donatur'].append('Anonymous')
    progCount = info['amount'] / 10000000  # pragma: no cover
    info['progress'] = str(progCount)  # pragma: no cover
    for j in allcampaigns:  # pragma: no cover
        if j.title == 'Bantu Korban Tsunami Palu-Donggala':
            info['title'] = j.title
            info['desc'] = j.description
    return render(request, 'campaignpage.html', info)


def campaign3(request):
    info = {}
    info['title'] = ''
    info['img'] = 'https://i.imgur.com/F91aG4Z.jpg'
    info['desc'] = ''
    info['amount'] = 0
    info['jmldonatur'] = 0
    info['donatur'] = []
    info['progress'] = ""  # pragma: no cover
    allevent = DonaturEvent.objects.all()  # pragma: no cover
    allcampaigns = Campaigns.objects.all()  # pragma: no cover
    for i in allevent:  # pragma: no cover
        if i.namaprogram == 'Bantu Anak-Anak Korban Gempa Lombok':  # pragma: no cover
            info['amount'] += i.amount  # pragma: no cover
            info['jmldonatur'] += 1  # pragma: no cover
            if i.allow:
                info['donatur'].append(i.nama)
            else:
                info['donatur'].append('Anonymous')  # pragma: no cover
    progCount = info['amount'] / 10000000  # pragma: no cover
    info['progress'] = str(progCount)  # pragma: no cover
    for j in allcampaigns:
        if j.title == 'Bantu Anak-Anak Korban Gempa Lombok':  # pragma: no cover
            info['title'] = j.title  # pragma: no cover
            info['desc'] = j.description  # pragma: no cover
    return render(request, 'campaignpage.html', info)
