from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

#allow_choices = [(1,'Allow to show my name as a donatur in the campaign')]

class DonaturEvent(models.Model):
	namaprogram = models.CharField(max_length=200)
	nama = models.CharField(max_length=200)
	email = models.CharField(max_length=200)
	amount = models.BigIntegerField(validators=[MinValueValidator(0), MaxValueValidator(999999999999999)])
	allow = models.BooleanField()
	time = models.DateTimeField(auto_now_add=True)
