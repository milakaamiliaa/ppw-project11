from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import DonaturEventForm
from .models import DonaturEvent

# Create your views here.
response = {}
def formdonation(request):    
    response['author'] = "" #TODO Implement yourname
    alldonatur = DonaturEvent.objects.all()
    response['alldonatur'] = alldonatur
    html = 'formdonation.html'
    response['donation_form'] = DonaturEventForm
    return render(request, html, response)

def adddonatur(request):
    form = DonaturEventForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['namaprogram'] = request.POST['namaprogram']
        response['nama'] = request.user.username
        response['email'] = request.user.email
        response['amount'] = request.POST['amount']
        if  'allow' in request.POST:
        	response['allow'] = True # pragma: no cover
        else:
        	response['allow'] = False
        donatur = DonaturEvent(namaprogram=response['namaprogram'], nama=response['nama'],
        	email=response['email'], amount=response['amount'], allow=response['allow'])
        donatur.save()
        return HttpResponseRedirect('/formdonation/donationsuccess')
    else:
        return HttpResponseRedirect('/formdonation')

def donationsuccess(request):
    return render(request, 'donationsuccess.html')
