from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .apps import DonationConfig
from django.http import HttpRequest
from django.apps import apps
from .models import DonaturEvent
from .forms import DonaturEventForm
from .views import *

class donation(TestCase):
	def test_donation_formdonation_url_is_exist(self):
		response = Client().get('/formdonation')
		self.assertEqual(response.status_code, 200)

	def test_donation_using_formdonation_template(self):
		response = Client().get('/formdonation')
		self.assertTemplateUsed(response, 'formdonation.html')

	def test_donation_using_formdonation_func(self):
		found = resolve('/formdonation')
		self.assertEqual(found.func, formdonation)

	def test_donation_using_adddonatur_func(self):
		found = resolve('/formdonation/adddonatur')
		self.assertEqual(found.func, adddonatur)

	def test_donation_donationsuccess_url_is_exist(self):
		response = Client().get('/formdonation/donationsuccess')
		self.assertEqual(response.status_code, 200)

	def test_donation_using_donationsuccess_func(self):
		found = resolve('/formdonation/donationsuccess')
		self.assertEqual(found.func, donationsuccess)

	def test_donation_model_can_create_new_donaturevent(self):
		new_donaturevent = DonaturEvent.objects.create(namaprogram='peduli palu', nama='feriyal bin yahya',
			email="feriyalbinyahya@gmail.com", amount=500000, allow=1)

		counting_all_available_donaturevent = DonaturEvent.objects.all().count()
		self.assertEqual(counting_all_available_donaturevent, 1)

	def test_form_donation_input_has_placeholder_and_css_classes(self):
		form = DonaturEventForm()
		self.assertIn('class="donation-form-input"', form.as_p())
		self.assertIn('id="id_donation"', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = DonaturEventForm(data={'namaprogram':'', 'nama':'', 'email':'', 'amount':0, 'allow':False})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['namaprogram'],["This field is required."])

	def test_donation_post_success_and_render_the_result(self):
		test_nama = 'Peduli gempa'
		test_amount = 100000
		response_post = Client().post('/formdonation', {'namaprogram': test_nama, 'nama': test_nama, 
			'email': test_nama, 'amount': test_amount, 'allow': True})
		self.assertEqual(response_post.status_code, 200)

	def test_donation_post_error_and_render_the_result(self):
		test = 'Anonymous'
		testamount = 100000
		response_post = Client().post('/formdonation', {'namaprogram': '', 'nama': '', 'email': '',
			'amount': 0, 'allow': True})
		self.assertEqual(response_post.status_code, 200)

	def test_donation_is_using_header_template(self):
		response = Client().get('/formdonation')
		self.assertTemplateUsed(response, 'header.html')

	def test_donation_is_using_footer_template(self):
		response = Client().get('/formdonation')
		self.assertTemplateUsed(response, 'footer.html')

	def test_apps(self):
		self.assertEqual(DonationConfig.name, 'donation')
		self.assertEqual(apps.get_app_config('donation').name, 'donation')
	