from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'donation'

urlpatterns = [
    path('formdonation', views.formdonation, name="formdonation"),
    path('formdonation/adddonatur', views.adddonatur, name="adddonatur"),
    path('formdonation/donationsuccess', views.donationsuccess, name="donationsuccess")
]