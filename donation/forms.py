from django import forms
from .models import DonaturEvent

class DonaturEventForm(forms.Form):
	text_attrs = {
		'id': 'id_donation',
		'type': 'text',
		'class': 'donation-form-input'
	}


	email_attrs = {
		'id': 'id_donation',
		'type': 'email',
		'class': 'donation-form-input'
	}

	number_attrs = {
		'id': 'id_donation',
		'type': 'number',
		'class': 'donation-form-input'
	}
	checkbox_attrs = {
		'id': 'id_checkbox',
		'type': 'checkbox',
		'class': 'donation-form-input'
	}


	namaprogram = forms.CharField(widget=forms.TextInput(attrs=text_attrs), label="", required=True)
	amount = forms.IntegerField(widget=forms.NumberInput(attrs=number_attrs), label="", required=True)
	allow = forms.BooleanField(widget=forms.CheckboxInput(attrs=checkbox_attrs), required=False)
