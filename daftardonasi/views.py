from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.forms.models import model_to_dict
from django.core import serializers
from django.contrib import messages
import requests
import json
from donation.models import DonaturEvent

# Create your views here.

def daftardonasi(request):
    if request.user.is_authenticated:
    	riwayat = list(DonaturEvent.objects.filter(email = request.user.email))
    	riwayat.reverse()
    	response = {
    		'riwayat' : riwayat
    	}
    	return render(request, 'daftardonasi.html', response)
    else:
    	notif = "Please login before accessing this page."
    	messages.warning(request, notif)
    	return HttpResponse('/profile/')
