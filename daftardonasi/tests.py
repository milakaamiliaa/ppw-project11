from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.apps import apps
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.


class DaftarDonasiLabUnitTest(TestCase):

    def test_daftardonasi_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_daftardonasi_using_daftardonasi_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, daftardonasi)

    # def test_daftardonasi_is_using_header_template(self):
    #     response = Client().get('/profile')
    #     self.assertTemplateUsed(response, 'header.html')
    #
    # def test_daftardonasi_is_using_footer_template(self):
    #     response = Client().get('/profile')
    #     self.assertTemplateUsed(response, 'footer.html')
    #
    # def test_daftardonasi_is_using_daftardonasi_template(self):
    #     response = Client().get('/profile')
    #     self.assertTemplateUsed(response, 'daftardonasi.html')
