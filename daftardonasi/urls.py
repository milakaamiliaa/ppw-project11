from django.urls import path
from django.conf.urls import url
from . import views
from .views import daftardonasi

app_name = 'daftardonasi'


urlpatterns = [
    path('profile/', views.daftardonasi, name='daftardonasi')
]
