from django.test import Client
from django.urls import resolve
from .apps import RegistersuccessConfig
from django.apps import apps
from .views import registersuccess
from django.test import TestCase

# Create your tests here

class RegisterSuccessUnitTest(TestCase):

    def test_if_registersuccess_is_using_header_template(self):
        response = Client().get('/register/success/')
        self.assertTemplateUsed(response, 'header.html')

    def test_if_register_is_using_footer_template(self):
        response = Client().get('/register/success/')
        self.assertTemplateUsed(response, 'footer.html')

    def test_apps(self):
        self.assertEqual(RegistersuccessConfig.name, 'registersuccess')
        self.assertEqual(apps.get_app_config('registersuccess').name, 'registersuccess')
