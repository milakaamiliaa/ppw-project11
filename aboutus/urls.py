from django.urls import path
from . import views
from .views import aboutus, testimoniPost

app_name = 'aboutus'

urlpatterns = [
    path('aboutus/', views.aboutus, name='aboutus'),
    path('testimonipost/', views.testimoniPost, name='testimonipost'),
]
