from django.test import TestCase

# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.apps import apps
from aboutus.apps import AboutusConfig
from .views import *


class logingoogle(TestCase):
    def test_aboutus_url_is_exist(self):
        response = Client().get('/aboutus/')
        self.assertEqual(response.status_code, 200)

    def test_aboutus_using_aboutus_func(self):
        found = resolve('/aboutus/')
        self.assertEqual(found.func, aboutus)

    def test_aboutus_app(self):
        self.assertEqual(AboutusConfig.name, 'aboutus')

    def test_testimonipost_success(self):
        response = Client().post('/testimonipost/', data={
            "nama": "agnes", "testimoni": "helo"
        })
        self.assertEqual(response.status_code, 200)

    def test_testimonipost_fail(self):
        response = Client().get('/testimonipost/')
        self.assertEqual(response.status_code, 200)
