from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.urls import reverse
from .models import TestimoniModel
import json
import requests
# Create your views here.

content = {}


def aboutus(request):
    content['testimonies'] = TestimoniModel.objects.all()
    return render(request, 'aboutus.html', content)


def testimoniPost(request):
    if (request.method == 'POST'):
        nama = request.POST['nama']
        testimoni = request.POST['testimoni']
        TestimoniModel.objects.create(nama=nama, testimoni=testimoni)
        now = list(TestimoniModel.objects.all().values())
        return JsonResponse({'saved': True, 'testimoni': testimoni, 'nama': nama, 'now': now})
    return JsonResponse({'saved': False})
