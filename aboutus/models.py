from django.db import models

# Create your models here.


class TestimoniModel(models.Model):
    nama = models.CharField(max_length=30)
    testimoni = models.TextField(max_length=300)
