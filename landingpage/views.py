from django.shortcuts import render
from .models import Campaigns
from .models import News

content = {}

def landingpage(request):
    content['campaign'] = Campaigns.objects.all()
    content['news'] = News.objects.all()
    return render(request, 'landingpage.html', content)