from django.urls import path
from . import views
from .views import landingpage

app_name = 'landingpage'

urlpatterns = [
	path('', views.landingpage, name = 'landingpage')
]
