from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .apps import LandingpageConfig
from django.apps import apps
from .views import landingpage
from .models import Campaigns
from .models import News

# Create your tests here.
class LandingPageUnitTest(TestCase):
    def test_landingpage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landingpage_using_landingpage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landingpage)

    def test_campaign_model_can_add_new(self):
        Campaigns.objects.create(image="", title="test", description="test")
        campaign_counts_all_entries = Campaigns.objects.all().count()
        self.assertEqual(campaign_counts_all_entries, 1)

    def test_news_model_can_add_new(self):
        News.objects.create(news_image="", news_title="test", news_description="test")
        news_counts_all_entries = News.objects.all().count()
        self.assertEqual(news_counts_all_entries, 1)

    def test_self_func_news(self):
        News.objects.create(news_image="", news_title="test", news_description="test")
        news = News.objects.get(news_title="test")
        self.assertEqual(str(news), news.news_title)

    def test_self_func_campaign(self):
        Campaigns.objects.create(image="", title="test", description="test")
        campaign = Campaigns.objects.get(title="test")
        self.assertEqual(str(campaign), campaign.title)

    def test_if_registersuccess_is_using_header_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'header.html')

    def test_if_register_is_using_footer_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'footer.html')

    def test_apps(self):
        self.assertEqual(LandingpageConfig.name, 'landingpage')
        self.assertEqual(apps.get_app_config('landingpage').name, 'landingpage')
