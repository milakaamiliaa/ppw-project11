from django.contrib import admin

from .models import Campaigns
admin.site.register(Campaigns)

from .models import News
admin.site.register(News)