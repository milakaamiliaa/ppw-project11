from django.db import models

class Campaigns(models.Model):
    image = models.ImageField()
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=300)

    def __str__(self):
        return self.title

class News(models.Model):
    news_image = models.ImageField()
    news_title = models.CharField(max_length=100)
    news_description = models.TextField(max_length=300)

    def __str__(self):
        return self.news_title