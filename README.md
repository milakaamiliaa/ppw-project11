# Project PPW-B Kelompok 11

## Yang beranggotakan :
 - Agnes Handoko (1706039761)
 - Firriyal Bin Yahya (1706979240)
 - Jose Devian Hibono (1706039603)
 - Siti Kaamiliaa Hasnaa (1706984732)

## Link HerokuApp
[https://ppw-project11.herokuapp.com/](https://ppw-project11.herokuapp.com/)

## Status Website
[![pipeline status](https://gitlab.com/milakaamiliaa/ppw-project11/badges/master/pipeline.svg)](https://gitlab.com/milakaamiliaa/ppw-project11/commits/master)
[![coverage report](https://gitlab.com/milakaamiliaa/ppw-project11/badges/master/coverage.svg)](https://gitlab.com/milakaamiliaa/ppw-project11/commits/master)

