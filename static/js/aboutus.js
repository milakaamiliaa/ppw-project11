$(document).ready(function () {



});

function sendTestimony() {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    console.log("masuk send testimoni");
    $.ajax({
        method: 'POST',
        url: "/testimonipost/",
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {
            nama: $('#nama').val(),
            testimoni: $('#testimoni').val(),
        },
        success: function (result) {
            if (result.saved) {

                var nama = result.nama;
                var testimoni = result.testimoni;
                var flexbox = '<div class="d-flex justify-content-center align-items-center flex-column testimony-set">';
                var flexboxclosed = '</div>';
                var show = flexbox + '<p class="testimonyText">"' + testimoni + '"</p><p><strong>' + nama + '</strong></p>' + flexboxclosed;
                $('#isiTestimoni').prepend(show)
                $('#testimoni').val('')
                // alert("Testimony posted")
                swal({
                    title: "Success",
                    text: "Thank you for your testimony",
                    type: 'success',
                    showConfirmButton: false,
                    timer: 1500,
                });


                // $('.isiTestimoni d-flex flex-wrap justify-content-center align-items-center').replaceWith('<div class="isiTestimoni d-flex flex-wrap justify-content-center align-items-center"></div>');
                // var length = result.now.length;
                // var data = result.now;
                // console.log(data);
                // for (var i = 0; i < length; i++) {
                //     var nama = data[i].nama;
                //     var testimoni = data[i].testimoni;
                //     var flexbox = '<div class="d-flex justify-content-center align-items-center flex-column testimony-set">';
                //     var flexboxclosed = '</div>';
                //     var show = flexbox + '<p class="testimonyText">' + testimoni + '</p><p><strong>' + nama + '</strong></p>' + flexboxclosed;
                //     $('.d-flex flex-wrap justify-content-center align-items-center').prepend(show)
                // }
                // $('#testimoni').val('')
                // alert("Testimony posted")
            }
        },
        error: function () {
            // alert("Can't post testimony")
            swal({
                type: 'error',
                title: 'Error',
                text: "Can't post testimony",
            });
        }
    })
};