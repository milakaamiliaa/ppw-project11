#!/usr/bin/env python
import os
import sys

if __name__ == '__main__':  # pragma: no cover
    os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                          'tp1.settings')  # pragma: no cover
    try:  # pragma: no cover
        from django.core.management import execute_from_command_line  # pragma: no cover
    except ImportError as exc:  # pragma: no cover
        raise ImportError(  # pragma: no cover
            "Couldn't import Django. Are you sure it's installed and "  # pragma: no cover
            "available on your PYTHONPATH environment variable? Did you "  # pragma: no cover
            "forget to activate a virtual environment?"  # pragma: no cover
        ) from exc  # pragma: no cover
    execute_from_command_line(sys.argv)  # pragma: no cover
