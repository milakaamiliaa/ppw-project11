from django.shortcuts import render, redirect
from .forms import RegistrationForm
from .models import Register

# Create your views here.

def register(request):
    form = RegistrationForm(request.POST or None)
    response = {}
    if request.method == "POST":
        if form.is_valid():
            data = form.cleaned_data
            name = request.POST.get('name')
            birthdate = request.POST.get('birthdate')
            email = request.POST.get('email')
            password = request.POST.get('password')
            Register.objects.create(**data)
            print(name, birthdate, email, password)
            return redirect('success/')

        else:
            return render(request, 'register.html', response)

    else:
        register = Register.objects.all()
        response['form'] = form
        response['register'] = register
        return render(request, 'register.html', response)
