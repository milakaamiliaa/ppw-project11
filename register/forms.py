from django import forms

class RegistrationForm(forms.Form):
    name = forms.CharField(label='Full Name\t:', required=True, max_length=100)
    birthdate = forms.DateField(label="Birth Date\t:", required=True, input_formats=['%Y-%m-%d'],
                           widget=forms.DateInput(attrs={'type': 'date'}))
    email = forms.EmailField(label='Email\t\t:', required=True, max_length=100)
    password = forms.CharField(label='Password\t:', required=True, widget=forms.PasswordInput)
