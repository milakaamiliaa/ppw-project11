from django.db import models

# Create your models here.

class Register(models.Model):
    name = models.CharField(max_length=100)
    birthdate = models.DateField(blank=True, null=True)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)

    def __str__(self):
        return self.name
