from django.test import Client
from django.urls import resolve
from .views import register
from .models import Register
from .forms import RegistrationForm
from django.test import TestCase
from .apps import RegisterConfig
from django.apps import apps

# Create your tests here

class RegisterUnitTest(TestCase):

    def test_url_register_is_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_using_register_function(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_form_post_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/register/', {'name': test, 'birthdate': '2000-12-01', 'email': test, 'password': test})
        self.assertEqual(response_post.status_code, 200)

    def test_form_validation_for_blank_item(self):
        form = RegistrationForm(data={'name': '', 'birthdate': '', 'email': '', 'password': ''})
        self.assertFalse(form.is_valid())

    def test_if_register_is_using_header_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'header.html')

    def test_if_register_is_using_footer_template(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'footer.html')

    def test_apps(self):
        self.assertEqual(RegisterConfig.name, 'register')
        self.assertEqual(apps.get_app_config('register').name, 'register')

    def test_self_func_form(self):
        Register.objects.create(name='test', birthdate='2000-12-01', email='johndoe@gmail.com', password='test')
        regis = Register.objects.get(name="test")
        self.assertEqual(str(regis), regis.name)

    def test_status_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/register/', {'name': test, 'birthdate': '2000-12-01', 'email': 'jose@gmail.com', 'password': 'haloaja'})
        self.assertEqual(response_post.status_code, 302)

        
